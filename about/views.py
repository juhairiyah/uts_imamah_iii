from multiprocessing import context
from django.http import HttpResponseRedirect
from django.urls import reverse
from turtle import title
from django.shortcuts import render
from .models import Obat


def index(request):
    context = {
        'obat_list' : Obat.objects.all()
    }
    return render(request, 'pertama.html', context)

def add(request):
    return render(request, 'form.html')

def save(request):
    obat = Obat(
        nama_obat = request.POST['nama_obat'],
        jumlah_obat = request.POST['jumlah_obat'],
        kategori_obat = request.POST['kategori_obat'],
        tanggal = request.POST['tanggal'],
    )
    obat.save()

    return HttpResponseRedirect(reverse('obat.index'))