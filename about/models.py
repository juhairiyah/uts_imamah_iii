from django.db import models

# Create your models here.


class Obat(models.Model):
    nama_obat = models.CharField(max_length=100)
    jumlah_obat = models.CharField(max_length=10)
    kategori_obat = models.CharField(max_length=10)
    tanggal = models.DateTimeField('tanggal beli')

    def __str__(self):
        return self.nama_obat