from django.urls import path

from . import views 

urlpatterns = [
    path('', views.index, name='obat.index'),
    path('add', views.add, name='obat.add'),
    path('save', views.save, name='obat.save'),
]